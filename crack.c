#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
    char *hashed = md5(guess,strlen(guess));
    //printf("%s\n",hash);
    // Compare the two hashes
    if(strncmp(hash, hashed, HASH_LEN) == 0){
        return 1;
    }else{
        return 0;
    }
    // Free any malloc'd memory
    free(hashed);
}

// Read in the dictionary file and return the array of strings
// and store the length of the array in size.
// This function is responsible for opening the dictionary file,
// reading from it, building the data structure, and closing the
// file.
int file_length(char *filename){
    struct stat fileinfo;
    if(stat(filename, &fileinfo) == -1){
        return -1;
    }else{
        return fileinfo.st_size;
    }
}

char ** read_dictionary(char *filename, int *size)
{
    *size = 0;
    int len = file_length(filename);
    if(len == -1){
        printf("couldnt get file length %s\n",filename);
        exit(1);
    }
    char *file_contents = malloc(len);
    
    FILE *fp1 = fopen(filename,"r");
    if(!fp1){
        printf("Cant open %s for reading\n",filename);
        exit(1);
    }
    fread(file_contents,1,len,fp1);
    fclose(fp1);
    
    int line_count = 0;
    for(int i = 0; i < len; i++){
        if(file_contents[i] == '\n'){
            file_contents[i] = '\0';
            line_count++;
        }
    }
    
    char **lines = malloc(line_count * sizeof(char *));
    
    int c = 0;
    for(int i = 0; i < line_count; i++){
        lines[i] = &file_contents[c];
        
        while(file_contents[c] != '\0') c++;
        c++;
    }
    *size = line_count;
    return lines;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Read the dictionary file into an array of strings.
    int dlen;
    char **dict = read_dictionary(argv[2], &dlen);

    // Open the hash file for reading.
    FILE *fp = fopen(argv[1],"r");
    if(!fp){
        printf("Cant open %s for reading\n",argv[1]);
        exit(1);
    }

    // For each hash, try every entry in the dictionary.
    // Print the matching dictionary entry.
    // Need two nested loops.
    char line[100];
    while(fgets(line,100,fp) != NULL){
        line[strlen(line)-1] = '\0';
        for(int j = 0; j<dlen; j++){
            
            int alpha = tryguess(line,dict[j]);

            if(alpha == 1){
                printf("%s matches %s\n",line,dict[j]);
            }
        }
    }
    free(dict[0]);
    free(dict);

}
